package com.example.spacetoonplayer.Responses;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.spacetoonplayer.Network.ApiClient;
import com.example.spacetoonplayer.Network.ApiService;
import com.example.spacetoonplayer.Repositories.TVShowDetailsRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTVShowRepository {
    private ApiService apiService;
    public SearchTVShowRepository(){
        apiService= ApiClient.getRetrofit().create(ApiService.class);
    }

    public LiveData<TvShowsResponse> searchTVShow(String query,int page){
        MutableLiveData<TvShowsResponse> data=new MutableLiveData<>();
        apiService.searchTVShow(query,page).enqueue(new Callback<TvShowsResponse>() {
            @Override
            public void onResponse(@NonNull Call<TvShowsResponse> call,@NonNull  Response<TvShowsResponse> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<TvShowsResponse> call, @NonNull Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }
}
