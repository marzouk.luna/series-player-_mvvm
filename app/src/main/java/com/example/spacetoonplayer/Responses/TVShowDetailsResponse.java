package com.example.spacetoonplayer.Responses;

import com.example.spacetoonplayer.Model.TVShowDetails;
import com.example.spacetoonplayer.TvShow;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TVShowDetailsResponse {

    @SerializedName("tvShow")
    private TVShowDetails tvShowDetails;

    public TVShowDetails getTvShowDetails() {
        return tvShowDetails;
    }
}
