package com.example.spacetoonplayer.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.spacetoonplayer.Repositories.MostPopularTVShowsRepository;
import com.example.spacetoonplayer.Responses.TvShowsResponse;

public class MostPopularTVShowsViewModel extends ViewModel {

    private MostPopularTVShowsRepository mostPopularTVShowsRepository;

    public MostPopularTVShowsViewModel() {
        mostPopularTVShowsRepository=new MostPopularTVShowsRepository();
    }

    public LiveData<TvShowsResponse> getMostPopularTVShows (int page){
        return  mostPopularTVShowsRepository.getMostPopularTVShows(page);
    }
}
