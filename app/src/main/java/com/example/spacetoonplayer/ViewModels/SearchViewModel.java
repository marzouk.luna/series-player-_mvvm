package com.example.spacetoonplayer.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.spacetoonplayer.Responses.SearchTVShowRepository;
import com.example.spacetoonplayer.Responses.TVShowDetailsResponse;
import com.example.spacetoonplayer.Responses.TvShowsResponse;

public class SearchViewModel extends ViewModel {
    private SearchTVShowRepository searchTVShowRepository;
    public SearchViewModel(){
        searchTVShowRepository=new SearchTVShowRepository();
    }
    public LiveData<TvShowsResponse> searchTVShow(String query, int page){
        return searchTVShowRepository.searchTVShow(query,page);
    }
}
