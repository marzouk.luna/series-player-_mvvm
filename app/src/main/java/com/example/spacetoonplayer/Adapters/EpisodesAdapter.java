package com.example.spacetoonplayer.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spacetoonplayer.Listenrs.EpisodeListener;
import com.example.spacetoonplayer.Model.Episode;
import com.example.spacetoonplayer.R;
import com.example.spacetoonplayer.databinding.ItemContainerEpisodeBinding;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.EpisodesViewHolder> {
    private List<Episode> episodes;

    private LayoutInflater layoutInflater;
    private EpisodeListener episodeListener;

    public EpisodesAdapter(List<Episode> episodes,EpisodeListener episodeListener) {
        this.episodes = episodes;
        this.episodeListener=episodeListener;
    }

    @NonNull
    @Override
    public EpisodesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(layoutInflater==null){
            layoutInflater=LayoutInflater.from(parent.getContext());
        }
        ItemContainerEpisodeBinding itemContainerEpisodeBinding= DataBindingUtil.inflate(
                layoutInflater, R.layout.item_container_episode,parent,false
        );
        return new EpisodesViewHolder(itemContainerEpisodeBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodesViewHolder holder, int position) {
        holder.bindEpisode(episodes.get(position));

    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }

     class EpisodesViewHolder extends RecyclerView.ViewHolder{

        private ItemContainerEpisodeBinding itemContainerEpisodeBinding;

        public EpisodesViewHolder( ItemContainerEpisodeBinding  itemContainerEpisodeBinding) {
            super(itemContainerEpisodeBinding.getRoot());
            this.itemContainerEpisodeBinding=itemContainerEpisodeBinding;
        }
        public void bindEpisode(Episode episode){
            String title="s";
            String season=episode.getSeason();
            if(season.length() ==1){
                season="0".concat(season);
            }
            String episodeNumber=episode.getEpisode();
            if(episodeNumber.length() ==1){
                episodeNumber="0".concat(episodeNumber);
            }
            episodeNumber="E".concat(episodeNumber);
            title=title.concat(season).concat(episodeNumber);
            itemContainerEpisodeBinding.setTitle(title);
            itemContainerEpisodeBinding.setName(episode.getName());
            itemContainerEpisodeBinding.setAirDate(episode.getAirDate());
            itemContainerEpisodeBinding.executePendingBindings();
            itemContainerEpisodeBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    episodeListener.onEpisodeClicked();
                }
            });
        }
    }
}
