package com.example.spacetoonplayer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.spacetoonplayer.Adapters.WatchlistAdapter;
import com.example.spacetoonplayer.Listenrs.WatchlistListener;
import com.example.spacetoonplayer.R;
import com.example.spacetoonplayer.TvShow;
import com.example.spacetoonplayer.Utilities.TempDataHolder;
import com.example.spacetoonplayer.ViewModels.WatchlistViewModel;
import com.example.spacetoonplayer.databinding.ActivityWatchlistBinding;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class WatchlistActivity extends AppCompatActivity implements WatchlistListener {

    private ActivityWatchlistBinding activityWatchlistBinding;
    private WatchlistViewModel viewModel;
    private WatchlistAdapter watchlistAdapter;
    private List<TvShow> watchlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityWatchlistBinding= DataBindingUtil.setContentView(this,R.layout.activity_watchlist);
        doInitialization();
    }

    private void doInitialization(){
        viewModel=new ViewModelProvider(this).get(WatchlistViewModel.class);
        activityWatchlistBinding.imageBack.setOnClickListener(v -> onBackPressed());
        watchlist=new ArrayList<>();
        loadWatchlist();
    }

    private  void loadWatchlist(){
        activityWatchlistBinding.setIsLoading(true);
        CompositeDisposable compositeDisposable=new CompositeDisposable();
        compositeDisposable.add(viewModel.loadWatchlist().subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
                tvShows -> {
                    activityWatchlistBinding.setIsLoading(false);
                   if(watchlist.size()>0){
                       watchlist.clear();
                   }
                   watchlist.addAll(tvShows);
                   watchlistAdapter=new WatchlistAdapter(watchlist,this);
                   activityWatchlistBinding.watchlistRecyclerView.setAdapter(watchlistAdapter);
                   activityWatchlistBinding.watchlistRecyclerView.setVisibility(View.VISIBLE);
                   compositeDisposable.dispose();
                }
        ));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(TempDataHolder.IS_WATCHLIST_UPDATES) {
            loadWatchlist();
            TempDataHolder.IS_WATCHLIST_UPDATES=false;
        }
    }

    @Override
    public void onTVShowClicked(TvShow tvShow) {
        Intent intent=new Intent(getApplicationContext(),TvShowDetailsActivity.class);
        intent.putExtra("tvShow",tvShow);
        startActivity(intent);
    }

    @Override
    public void removeTVShowsFromWatchlist(TvShow tvShow, int position) {

        CompositeDisposable compositeDisposableForDelete=new CompositeDisposable();
        compositeDisposableForDelete.add(viewModel.removeTVShowFromWatchlist(tvShow)
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
                ()->{
                    watchlist.remove(position);
                    watchlistAdapter.notifyItemRemoved(position);
                    watchlistAdapter.notifyItemRangeChanged(position,watchlistAdapter.getItemCount());
                    compositeDisposableForDelete.dispose();
                }
        ));
    }
}