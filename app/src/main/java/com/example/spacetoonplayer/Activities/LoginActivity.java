package com.example.spacetoonplayer.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.spacetoonplayer.MainActivity;
import com.example.spacetoonplayer.R;

public class LoginActivity extends AppCompatActivity {
    private  String email,password;
    private Button btnLogin;
    private EditText edEmail,edPassword;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences=this.getSharedPreferences("login",MODE_PRIVATE);
        editor=sharedPreferences.edit();

        if(sharedPreferences.getString("isLogin","false").equals("yes"))
            openDash();
        edEmail=findViewById(R.id.edEmailLogin);
        edPassword=findViewById(R.id.edPasswordLogin);
        btnLogin=findViewById(R.id.cirLoginButton);

        edEmail.setAnimation(AnimationUtils.loadAnimation(this,R.anim.fade_transition_animation));
        edPassword.setAnimation(AnimationUtils.loadAnimation(this,R.anim.fade_transition_animation));
        btnLogin.setAnimation(AnimationUtils.loadAnimation(this,R.anim.fade_transition_animation));

    }

    private void openDash() {
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
        finish();
    }

    public void validateData(View view) {
        email=edEmail.getText().toString();
        password=edPassword.getText().toString();

        if(email.isEmpty()){
            edEmail.setError("Required");
            edEmail.requestFocus();
        }
        else if(password.isEmpty()){
            edPassword.setError("Required");
            edPassword.requestFocus();
        }
        else if(email.equals("admin@gmail.com") && password.equals("12345678")){
            editor.putString("isLogin","yes");
            editor.commit();
            openDash();
        }
        else{
            Toast.makeText(this, "please checl email and password agin",Toast.LENGTH_LONG).show();
        }
    }
}