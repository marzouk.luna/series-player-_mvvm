package com.example.spacetoonplayer.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.spacetoonplayer.Adapters.TVShowsAdapter;
import com.example.spacetoonplayer.Listenrs.TVShowsListener;
import com.example.spacetoonplayer.TvShow;
import com.example.spacetoonplayer.R;
import com.example.spacetoonplayer.ViewModels.MostPopularTVShowsViewModel;
import com.example.spacetoonplayer.databinding.ActivityTvListSeriesBinding;

import java.util.ArrayList;
import java.util.List;

public class TvListSeries extends AppCompatActivity implements TVShowsListener {

    private ActivityTvListSeriesBinding activityTvListSeriesBinding;
    private MostPopularTVShowsViewModel viewModel;
    private List<TvShow> tvShows=new ArrayList<>();
    private TVShowsAdapter tvShowsAdapter;
    private int currentPage=1;
    private int totalAvailablePages=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityTvListSeriesBinding= DataBindingUtil.setContentView(this,R.layout.activity_tv_list_series);
       doInitialization();

    }
    private  void doInitialization(){
        activityTvListSeriesBinding.tvShowsRecyclerView.setHasFixedSize(true);
        viewModel=new ViewModelProvider(this).get(MostPopularTVShowsViewModel.class);
        tvShowsAdapter=new TVShowsAdapter(tvShows,this);
        activityTvListSeriesBinding.tvShowsRecyclerView.setAdapter(tvShowsAdapter);
        activityTvListSeriesBinding.tvShowsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!activityTvListSeriesBinding.tvShowsRecyclerView.canScrollVertically(1)){
                    if(currentPage<=totalAvailablePages){
                        currentPage+=1;
                        getMostPopularTVShows();
                    }
                }
            }
        });
        activityTvListSeriesBinding.imageWatchlist.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(),WatchlistActivity.class)));
        activityTvListSeriesBinding.imageSearch.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(),SearchActivity.class)));
        getMostPopularTVShows();
    }
    private void getMostPopularTVShows(){
        toggleLoading();
        viewModel.getMostPopularTVShows(currentPage).observe(this,mostPopularTVShowsResponse ->
                { toggleLoading();
                    if(mostPopularTVShowsResponse !=null){
                        totalAvailablePages=mostPopularTVShowsResponse.getTotalPages();
                        if(mostPopularTVShowsResponse.getTvShows()!= null){
                            int oldCount=tvShows.size();
                            tvShows.addAll(mostPopularTVShowsResponse.getTvShows());
                            tvShowsAdapter.notifyItemRangeInserted(oldCount,tvShows.size());
                        }
                    }
                }
                );

    }
    private void toggleLoading(){
        if (currentPage==1){
            if (activityTvListSeriesBinding.getIsLoading()!=null && activityTvListSeriesBinding.getIsLoading()){
                activityTvListSeriesBinding.setIsLoading(false);
            }else {
                activityTvListSeriesBinding.setIsLoading(true);
            }
        }else {
            if (activityTvListSeriesBinding.getIsLoadingMore()!=null && activityTvListSeriesBinding.getIsLoadingMore()){
                activityTvListSeriesBinding.setIsLoadingMore(false);
            }else {
                activityTvListSeriesBinding.setIsLoadingMore(true);
            }
        }
    }

    @Override
    public void onTVShowClicked(TvShow tvShow) {
        Intent intent=new Intent(getApplicationContext(),TvShowDetailsActivity.class);
        intent.putExtra("tvShow",tvShow);
        startActivity(intent);
    }
}