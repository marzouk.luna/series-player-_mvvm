package com.example.spacetoonplayer.Listenrs;

import com.example.spacetoonplayer.TvShow;

public interface EpisodeListener {
    void onEpisodeClicked();
}
