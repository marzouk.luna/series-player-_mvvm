package com.example.spacetoonplayer.Listenrs;

import com.example.spacetoonplayer.TvShow;

public interface TVShowsListener {
    void onTVShowClicked(TvShow tvShow);

}
