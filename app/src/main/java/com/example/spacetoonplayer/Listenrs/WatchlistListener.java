package com.example.spacetoonplayer.Listenrs;

import com.example.spacetoonplayer.TvShow;

public interface WatchlistListener {

    void onTVShowClicked(TvShow tvShow);

    void removeTVShowsFromWatchlist(TvShow tvShow,int position);

}
